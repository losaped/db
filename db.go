package db

import (
	"errors"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var session *mgo.Session
var ci *mgo.DialInfo

// Connect connects to mongodb
func Connect(conString string) {
	var err error
	ci, err = mgo.ParseURL(conString)
	if err != nil {
		panic(err)
	}

	ci.Timeout = time.Duration(5 * time.Second)
	s, err := mgo.DialWithInfo(ci)
	if err != nil {
		panic(err)
	}

	session = s
	session.SetSafe(&mgo.Safe{})
}

// Exec выполняет функцию qf над коллекцией с переданным именем collection
func Exec(collection string, qf func(*mgo.Collection) error) error {
	if session == nil {
		return errors.New("Нет соединения с монго")
	}
	ls := session.Clone()
	defer ls.Close()

	database := ls.DB(ci.Database)
	col := database.C(collection)
	return qf(col)
}

// QueryOptions Структура для параметров запроса
type QueryOptions struct {
	Filter bson.M
	Limit  int
	Skip   int
}

// Select recieve pointer on a relult slice and filling him
func (qo QueryOptions) Select(collectionName string, result interface{}) error {
	return Exec(collectionName, func(c *mgo.Collection) error {

		query := c.Find(qo.Filter)

		if qo.Limit > 0 {
			query.Limit(qo.Limit)
		}

		if qo.Skip > 0 {
			query.Skip(qo.Skip)
		}

		return query.All(result)
	})
}

// SelectOne recieve pointer on a relult variable
func (qo QueryOptions) SelectOne(collectionName string, result interface{}) error {
	return Exec(collectionName, func(c *mgo.Collection) error {

		query := c.Find(qo.Filter)

		if qo.Limit > 0 {
			query.Limit(qo.Limit)
		}

		if qo.Skip > 0 {
			query.Skip(qo.Skip)
		}

		return query.One(result)
	})
}
