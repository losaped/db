package db

import (
	"gopkg.in/mgo.v2/bson"
)

type Query struct {
	Filter bson.M
	Limit  int
	Skip   int
}
